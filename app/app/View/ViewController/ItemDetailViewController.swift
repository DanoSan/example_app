//
//  ItemDetailViewController.swift
//  app
//
//  Created by Dario Ciaudano on 18/07/22.
//

import UIKit

class ItemDetailViewController: UIViewController {
    
    //MARK: - IBoutlets
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var cover: UIImageView!
    @IBOutlet weak var buyBtn: CustomButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var summary: UILabel!
    @IBOutlet weak var summaryExtension: UILabel!
    @IBOutlet weak var summaryStackView: UIStackView!
    @IBOutlet weak var loadMoreSummaryButton: UIButton!
    @IBOutlet weak var rights: UILabel!


    
    //MARK: - Private properties
    var viewModel: ItemViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    private func setup() {
        self.name.text = self.viewModel?.name
        self.author.text = self.viewModel?.artist
        self.viewModel?.fetchCoverImage {[weak self] (result) in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                switch(result) {
                case .success(let data):
                    self.cover.image = UIImage(data: data)
                case .failure(let error):
                    self.cover.image = nil
                    print(error.description)
                }
            }
        }
        
        self.price.text = self.viewModel?.price
        self.releaseDate.text = self.viewModel?.releaseDate
        
        var summarText = self.viewModel?.summary.split(separator: "\n", maxSplits: 1, omittingEmptySubsequences: false)

        if let minSummary = summarText?.removeFirst() {
            self.summary.text = String(minSummary)
            if let longSummary = summarText?.first {
                self.summaryExtension.text = String(longSummary)
                self.loadMoreSummaryButton.isHidden = false
            } else {
                self.summaryExtension.text = nil
                self.loadMoreSummaryButton.isHidden = true
            }
        } else {
            self.summary.text = "Currently not Available."
        }
        
        self.rights.text = self.viewModel?.rights
        
        self.buyBtn.onDetailPressed = {[weak self] () in
            DispatchQueue.main.async {
                self?.showPopup()
            }
        }
    }
    
    
    @IBAction func tapInsideAction(_ sender: Any) {
        self.loadMoreSummaryButton.isHidden = true
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0.0,
            usingSpringWithDamping: 0.9,
            initialSpringVelocity: 1,
            options: [],
            animations: {[weak self] () in
                self?.summaryExtension.isHidden = false
                self?.summaryStackView.layoutIfNeeded()
            },
            completion: nil
        )
    }
    
    
    private func showPopup() {
        let alert = UIAlertController(
            title: "Info",
            message: "This functionality is not currently available.",
            preferredStyle: .alert
        )
                
        alert.addAction(UIAlertAction(
            title: "OK",
            style: .default
        ))
        
        self.present(alert, animated: true)
    }
}
