//
//  ItemCellViewController.swift
//  app
//
//  Created by Dario Ciaudano on 18/07/22.
//

import UIKit

class ItemCellViewController: UITableViewCell {

    //MARK: - Outlet Objects
    @IBOutlet weak var cover: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var amount: UILabel!

    
    //MARK: - Private properties
    private var viewModel: ItemViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: Cell Setup
    /// Initialize the cell
    func setup(with viewModel: ItemViewModel) {
        self.viewModel = viewModel
        
        self.title.text = viewModel.name
        self.amount.text = viewModel.price
        viewModel.fetchCoverImage {[weak self] (result) in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                switch(result) {
                case .success(let data):
                    self.cover.image = UIImage(data: data)
                case .failure(let error):
                    self.cover.image = nil
                    print(error.description)
                }
            }
        }
    }
}
