//
//  MainViewModel.swift
//  app
//
//  Created by Dario Ciaudano on 18/07/22.
//

import Foundation

class MainViewModel {
    private var list: [AppModel]
    private var filteredList: [AppModel]
    private var listenerFilteredList: (([AppModel]) -> Void)?
    
    private var networkEngine: NetworkEngine
    
    private let elementsLimit: Int
    private let elementsNumBatch: Int?

    private var isPaginating: Bool
    
    
    init(_ networkEngine: NetworkEngine, elementsLimit: Int = 0, usePaginationWith elementsNumBatch: Int? = nil) {
        self.list = []
        self.filteredList = []
        
        self.networkEngine = networkEngine
        
        self.elementsLimit = elementsLimit
        self.elementsNumBatch = elementsNumBatch
        
        self.isPaginating = false
    }
    
    
    //MARK: - Use Cases
    func bindElements(listener: @escaping (([AppModel]) -> Void)) {
        self.listenerFilteredList?(self.filteredList)
        self.listenerFilteredList = listener
    }
    
    
    func fetchNewElementsBatch() -> Bool {
        if !self.isPaginating {
            if self.elementsLimit > self.list.count {
                return true
            }
        }
        return false
    }
    
    func retrieveContent(completion: ((Bool) -> Void)? = nil) {
        self.isPaginating = true
        
        var size = self.elementsLimit - self.list.count
        if let batchSize = self.elementsNumBatch,
           size >= batchSize {
            size = batchSize
        }

        self.retrieveContent(size: size)
        {[weak self] (result) in
            guard let self = self else {
                self?.isPaginating = false
                completion?(false)
                return
            }
            
            switch(result) {
            case .success(let data):
                do {
                    self.list = try JSONDecoder().decode(AppModelNetwork.self, from: data).convertToAppModelList
                    self.accept(filteredList: self.list)
                    self.isPaginating = false
                    completion?(true)
                } catch(let error) {
                    self.isPaginating = false
                    completion?(false)
                }
            case .failure(let error):
                self.isPaginating = false
                //TODO: Manage error
                completion?(false)
            }
        }
    }
    
    
    func retrieveElement(at pos: Int) -> AppModel? {
        if self.filteredList.count > pos {
            return self.filteredList[pos]
        } else {
            return nil
        }
    }
    
    var totalElements: Int {
        get {
            return self.filteredList.count
        }
    }
    
    func filterListWith(_ text: String) {
        self.accept(filteredList: self.list.filter { $0.name.contains(text) })
    }
    
    
    
    
    //MARK: - Private methods
    private func retrieveContent(size: Int, completion: @escaping ((Result<Data, CustomError>) -> Void)) {
        self.networkEngine.request(
            path: NetworkService.retrieveTopApplication(limit: size),
            method: .GET)
        { (result) in
            switch(result) {
            case .success((_, let data)):
                completion(.success(data))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private func accept(filteredList: [AppModel]) {
        self.filteredList = filteredList
        self.listenerFilteredList?(filteredList)
    }
}
