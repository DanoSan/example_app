//
//  MainViewController.swift
//  app
//
//  Created by Dario Ciaudano on 18/07/22.
//

import UIKit

class MainViewController: UIViewController {
    
    //MARK: - IBOutlet
    @IBOutlet weak var searchBarView: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: - Private properties
    var viewModel: MainViewModel?
    var host: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }


    private func setup() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchBarView.delegate = self
        
        self.viewModel?.bindElements {[weak self] _ in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        self.viewModel?.retrieveContent()
    }
}


extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.totalElements ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let vm = self.viewModel,
           let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as? ItemCellViewController,
           let element = vm.retrieveElement(at: indexPath.row) {
            cell.setup(with: ItemViewModel(element: element, vm.networkEngine))
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let identifier: String = "ItemDetail"
        if let element = self.viewModel?.retrieveElement(at: indexPath.row),
           let vc = UIStoryboard(name: identifier, bundle: nil)
            .instantiateViewController(withIdentifier: identifier) as? ItemDetailViewController {
            vc.viewModel = element
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            //TODO: Show some error
        }
    }
    
}


extension MainViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
        if abs(position) > abs(self.tableView.contentSize.height-scrollView.frame.size.height) {
            if self.viewModel?.fetchNewElementsBatch() ?? false {
                self.searchBarView.text = ""
                self.addElements()
            }
        }
    }
    
    private func addElements() {
        DispatchQueue.main.async {
            self.tableView.tableFooterView = self.createFooterView()
        }
        
        self.viewModel?.retrieveContent {[weak self] (success) in
            guard let self = self else { return }
            
            if success {
                DispatchQueue.main.async {
                    self.tableView.tableFooterView = nil
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    
    private func createFooterView() -> UIView {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 100))
        let spinner = UIActivityIndicatorView()
        spinner.center = footerView.center
        footerView.addSubview(spinner)
        spinner.startAnimating()

        return footerView
    }
}

extension MainViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel?.filterListWith(searchText)
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.viewModel?.filterListWith("")
        self.searchBarView.text = ""
        self.tableView.reloadData()
    }
}
