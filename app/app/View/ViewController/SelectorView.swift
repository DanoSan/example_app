//
//  SelectorView.swift
//  app
//
//  Created by Dario Ciaudano on 19/07/22.
//

import UIKit

enum SelectorStatus {
    case left
    case right
    
    mutating func toggle() {
        switch(self) {
        case .left:
            self = .right
        case .right:
            self = .left
        }
    }
    
    var direction: CGFloat {
        get {
            switch(self) {
            case .left:
                return 1
            case .right:
                return 0
            }
        }
    }
}


protocol SelectorViewDelegate {
    func onChangeSelection(with sectionSelected: SelectorStatus)
}



@IBDesignable
class SelectorView: UIView {
    let className: String

    //MARK: - Inspectable
    @IBInspectable var textLeft: String = "" {
       didSet {
           self.labelLeft.text = self.textLeft
       }
    }
    
    @IBInspectable var textRight: String = "" {
       didSet {
           self.labelRight.text = self.textRight
       }
    }
    
    
    
    //MARK: - IBOutlet
    @IBOutlet weak var slidingView: UIView!
    @IBOutlet weak var labelLeft: UILabel!
    @IBOutlet weak var labelRight: UILabel!
    
    
    //MARK: - Action
    @IBAction func tapGesture(sender: UITapGestureRecognizer) {
        DispatchQueue.main.async {
            switch (sender.state) {
            case .possible:
                self.isUserInteractionEnabled = false
                UIView.animate(
                    withDuration: 0.5,
                    delay: 0.0,
                    options: [.curveEaseInOut, .allowUserInteraction],
                    animations: {[weak self] () in
                        guard let self = self else { return }
                        self.slidingView.transform = CGAffineTransform(translationX: self.slidingView.frame.width * self.currentStatus.direction, y: 0)
                    },
                   completion: {[weak self] _ in
                       guard let self = self else { return }
                       self.currentStatus.toggle()
                       self.delegate?.onChangeSelection(with: self.currentStatus)
                       self.isUserInteractionEnabled = true
                   })
            default:
                break
            }
        }
        
        self.onDetailPressed?()
    }
    
    var onDetailPressed : (() -> Void)?
    
    var currentStatus: SelectorStatus = .left

    var delegate: SelectorViewDelegate?
    
    
    
    //MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        self.className = String(describing: SelectorView.self)

        super.init(coder: aDecoder)
        self.setup()
    }

    override init(frame: CGRect) {
        self.className = String(describing: SelectorView.self)

        super.init(frame: frame)
        self.setup()
    }

    
    //MARK: - Setup XIB
    private func setup() {
        guard let nib = loadNib() else { return }
        nib.translatesAutoresizingMaskIntoConstraints = false
        addSubview(nib)
        NSLayoutConstraint.activate([
            nib.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            nib.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            nib.topAnchor.constraint(equalTo: self.topAnchor),
            nib.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    

    //MARK: - load nib from xib
    func loadNib() -> UIView? {
        let bundle = Bundle(for: Self.self)
        return bundle.loadNibNamed(String(describing: Self.self), owner: self, options: nil)?.first as? UIView
    }
}
