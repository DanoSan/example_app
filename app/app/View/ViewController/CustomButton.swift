//
//  CustomButton.swift
//  app
//
//  Created by Dario Ciaudano on 19/07/22.
//


import UIKit


@IBDesignable
class CustomButton: UIView {
    let className: String

    //MARK: - Inspectable
    @IBInspectable var text: String = "" {
       didSet {
           self.label.text = self.text
       }
    }
    
    @IBInspectable var backgroundButtonColor: UIColor = UIColor() {
       didSet {
           self.backgroundColor = self.backgroundButtonColor
       }
    }
    
    
    
    //MARK: - IBOutlet
    @IBOutlet weak var label: UILabel!
    
    
    //MARK: - Action
    @IBAction func tapGesture(sender: UITapGestureRecognizer) {
        DispatchQueue.main.async {
            switch (sender.state) {
            case .possible:
                self.isUserInteractionEnabled = false
                self.alpha = 0.4
                UIView.animate(
                    withDuration: 0.5,
                    delay: 0.0,
                    options: [.curveEaseInOut, .allowUserInteraction],
                    animations: {[weak self] () in
                        self?.alpha = 1.0
                    },
                   completion: {[weak self] _ in
                       self?.isUserInteractionEnabled = true
                   })
            default:
                break
            }
        }
        
        self.onDetailPressed?()
    }
    
    var onDetailPressed : (() -> Void)?
        
    
    //MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        self.className = String(describing: CustomButton.self)

        super.init(coder: aDecoder)
        self.setup()
    }

    override init(frame: CGRect) {
        self.className = String(describing: CustomButton.self)

        super.init(frame: frame)
        self.setup()
    }

    
    //MARK: - Setup XIB
    private func setup() {
        guard let nib = loadNib() else { return }
        nib.translatesAutoresizingMaskIntoConstraints = false
        addSubview(nib)
        NSLayoutConstraint.activate([
            nib.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            nib.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            nib.topAnchor.constraint(equalTo: self.topAnchor),
            nib.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    

    //MARK: - load nib from xib
    func loadNib() -> UIView? {
        let bundle = Bundle(for: Self.self)
        return bundle.loadNibNamed(String(describing: Self.self), owner: self, options: nil)?.first as? UIView
    }
}
