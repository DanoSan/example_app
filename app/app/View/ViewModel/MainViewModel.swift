//
//  MainViewModel.swift
//  app
//
//  Created by Dario Ciaudano on 18/07/22.
//

import Foundation


class MainViewModel {
    private var list: [AppModel]
    private var filteredList: [AppModel]
    private var listenerFilteredList: (([AppModel]) -> Void)?
    private var filterType: SelectorStatus
    var debounceTimer: Timer?
    
    var networkEngine: NetworkEngine
    
    private let elementsLimit: Int
    private let elementsNumBatch: Int?
        
    private var isPaginating: Bool
    private var isListFiltered: Bool
    
    
    init(_ networkEngine: NetworkEngine, elementsLimit: Int = 0, usePaginationWith elementsNumBatch: Int? = nil) {
        self.list = []
        self.filteredList = []
        self.filterType = .left
        
        self.networkEngine = networkEngine
        
        self.elementsLimit = elementsLimit
        self.elementsNumBatch = elementsNumBatch
        
        self.isPaginating = false
        self.isListFiltered = false
    }
    
    
    //MARK: - Use Cases
    func bindElements(listener: @escaping (([AppModel]) -> Void)) {
        self.listenerFilteredList?(self.filteredList)
        self.listenerFilteredList = listener
    }
    
    
    func fetchNewElementsBatch() -> Bool {
        var needToRefresh = false
        if !self.isPaginating && !self.isListFiltered && elementsNumBatch != nil {
            if self.elementsLimit > self.list.count {
                needToRefresh = true
            }
        }
        return needToRefresh
    }
    
    func retrieveContent(completion: ((Bool) -> Void)? = nil) {
        self.isPaginating = true
        
        var size = self.elementsLimit - self.list.count
        if let batchSize = self.elementsNumBatch,
           size >= batchSize {
            size = batchSize
        }

        self.retrieveContent(size: size)
        {[weak self] (result) in
            guard let self = self else {
                self?.isPaginating = false
                completion?(false)
                return
            }
            
            switch(result) {
            case .success(let data):
                do {
                    self.list = try JSONDecoder().decode(AppModelNetwork.self, from: data).convertToAppModelList
                    self.accept(filteredList: self.list)
                    self.isPaginating = false
                    completion?(true)
                } catch(let error) {
                    self.isPaginating = false
                    print(error.localizedDescription)
                    completion?(false)
                }
            case .failure(let error):
                self.isPaginating = false
                print(error.description)
                completion?(false)
            }
        }
    }
    
    
    func retrieveElement(at pos: Int) -> AppModel? {
        if self.filteredList.count > pos {
            return self.filteredList[pos]
        } else {
            return nil
        }
    }
    
    var totalElements: Int {
        get {
            return self.filteredList.count
        }
    }
    
    func filterListWith(_ text: String) {
        let filteredList: [AppModel]
        if text.count > 0 {
            self.isListFiltered = true
            switch(self.filterType) {
            case .left:
                filteredList = self.list.filter { $0.name.lowercased().contains(text.lowercased()) }
            case .right:
                filteredList = self.list.filter { $0.category.lowercased().contains(text.lowercased()) }
            }
        } else {
            self.isListFiltered = false
            filteredList = self.list
        }
        self.accept(filteredList: filteredList)
    }
    
    func changeFilterType(filter: SelectorStatus, with currentFilteredText: String?) {
        self.filterType = filter
        
        self.filterListWith(currentFilteredText ?? "")
    }
    
    
    
    
    //MARK: - Private methods
    private func retrieveContent(size: Int, completion: @escaping ((Result<Data, CustomError>) -> Void)) {
        self.networkEngine.request(
            path: NetworkService.retrieveTopApplication(limit: size),
            method: .GET)
        { (result) in
            switch(result) {
            case .success((_, let data)):
                completion(.success(data))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private func accept(filteredList: [AppModel]) {
        self.filteredList = filteredList
        self.listenerFilteredList?(filteredList)
    }
}
