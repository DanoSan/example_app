//
//  ItemViewModel.swift
//  app
//
//  Created by Dario Ciaudano on 19/07/22.
//

import Foundation

class ItemViewModel {
    
    private var element: AppModel
    private var networkEngine: NetworkEngine

    
    init(element: AppModel, _ networkEngine: NetworkEngine) {
        self.element = element
        self.networkEngine = networkEngine
    }
    
    
    
    
    //MARK: - Get-only properties
    var name: String {
        get {
            return self.element.name
        }
    }
    
    var price: String {
        get {
            return String(format: "Price: %@", "\(self.element.amount) \(self.currencySymbol)")
        }
    }
    
    var artist: String {
        get {
            return self.element.artist
        }
    }
    
    var summary: String {
        get {
            return self.element.summary
        }
    }
    
    var rights: String {
        get {
            return self.element.rights
        }
    }
    
    var releaseDate: String {
        get {
            return String(format: "Publish date: %@", self.element.releaseDate)
        }
    }
    
    
    
    
    
    private var currencySymbol: String {
        get {
            let defaultCurrency = "$"
            
            let locale = NSLocale(localeIdentifier: self.element.currency)
            if locale.displayName(forKey: .currencySymbol, value: self.element.currency) == self.element.currency {
                let newlocale = NSLocale(localeIdentifier: self.element.currency.dropLast() + "_en")
                return newlocale.displayName(forKey: .currencySymbol, value: self.element.currency) ?? defaultCurrency
            }
            
            return locale.displayName(forKey: .currencySymbol, value: self.element.currency) ?? defaultCurrency
        }
    }
    
    
    func fetchCoverImage(completion: ((Result<Data, CustomError>) -> Void)?) {
        self.networkEngine.request(url: self.element.image, method: .GET) { (result) in
            switch(result) {
            case .success((_, let data)):
                completion?(.success(data))
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
}
