//
//  NetworkEngine.swift
//  app
//
//  Created by Dario Ciaudano on 18/07/22.
//

import Foundation

class NetworkEngine {
    private var host: String
    private var queue: DispatchQueue
    private var currentTask: URLSessionTask?
    
    init(host: String) {
        self.host = host
        self.queue = DispatchQueue(label: "network.request.queue", qos: .background, target: .global())
        self.currentTask = nil
    }
    
    
    
    func request(
        host: String? = nil,
        path: NetworkService,
        method: NetworkMethod,
        headers: [String:Any]? = nil,
        body: Data? = nil,
        completion: ((Result<(Int, Data), CustomError>) -> Void)?)
    {
        self.request(
            completePath: (host ?? self.host) + path.servive,
            method: method,
            headers: headers,
            body: body,
            completion: completion
        )
    }
    
    
    func request(
        completePath: String,
        method: NetworkMethod,
        headers: [String:Any]? = nil,
        body: Data? = nil,
        completion: ((Result<(Int, Data), CustomError>) -> Void)?)
    {
        if let url = URL(string: completePath) {
            self.request(
                url: url,
                method: method,
                headers: headers,
                body: body,
                completion: completion
            )
        } else {
            completion?(.failure(.unableToParseURL(url: completePath)))
        }
    }
    
    
    
    func request(
        url: URL,
        method: NetworkMethod,
        headers: [String:Any]? = nil,
        body: Data? = nil,
        completion: ((Result<(Int, Data), CustomError>) -> Void)?)
    {
            self.currentTask = URLSession.shared.dataTask(with: url) {(data, response, error) in
                if let e = error {
                    completion?(.failure(.error(desc: e.localizedDescription)))
                } else {
                    if let r = (response as? HTTPURLResponse),
                       let data = data {
                        if r.statusCode >= 200 && r.statusCode < 300 {
                            completion?(.success((r.statusCode, data)))
                        } else {
                            completion?(.failure(.errorWith(code: r.statusCode, desc: r.description)))
                        }
                    } else {
                        completion?(.failure(.unexpectedError))
                    }
                }
                
            }
        self.currentTask?.resume()
    }
}
