//
//  AppModelNetwork.swift
//  app
//
//  Created by Dario Ciaudano on 18/07/22.
//

import Foundation


fileprivate struct TextElement: Decodable {
    var label: String
}


fileprivate struct ResourceElement: Decodable {
    struct Attributes: Decodable {
        var height: String
    }
    
    var label: String
    var attributes: Attributes?
}

fileprivate struct CurrencyElement: Decodable {
    struct Attributes: Decodable {
        var amount: String
        var currency: String
    }
    
    var label: String
    var attributes: Attributes?
}

fileprivate struct ContentType: Decodable {
    struct Attributes: Decodable {
        var term: String
        var label: String
    }
    
    var attributes: Attributes?
}

fileprivate struct IdentifierElement: Decodable {
    struct Attributes: Decodable {
        var id: String
        var bundleId: String
        
        private enum CodingKeys: String, CodingKey {
            case id = "im:id"
            case bundleId = "im:bundleId"
        }
    }
    
    var label: String
    var attributes: Attributes?
}

fileprivate struct ArtistElement: Decodable {
    struct Attributes: Decodable {
        var href: String
    }
    
    var label: String
    var attributes: Attributes?
}

fileprivate struct CategoryElement: Decodable {
    struct Attributes: Decodable {
        var id: String
        var term: String
        var scheme: String
        var label: String
        
        private enum CodingKeys: String, CodingKey {
            case id = "im:id"
            case term = "term"
            case scheme = "scheme"
            case label = "label"
        }
    }
    
    var attributes: Attributes?
}

fileprivate struct ReleaseDateElement: Decodable {
    var label: String
    var attributes: TextElement
}



fileprivate struct AppModelDecodable: Decodable {
    var name: TextElement
    var image: [ResourceElement]
    var summary: TextElement
    var price: CurrencyElement
    var contentType: ContentType
    var rights: TextElement
    var title: TextElement
    var id: IdentifierElement
    var artist: ArtistElement
    var category: CategoryElement
    var releaseDate: ReleaseDateElement
    
    private enum CodingKeys: String, CodingKey {
        case name = "im:name"
        case image = "im:image"
        case summary = "summary"
        case price = "im:price"
        case contentType = "im:contentType"
        case rights = "rights"
        case title = "title"
        case id = "id"
        case artist = "im:artist"
        case category = "category"
        case releaseDate = "im:releaseDate"
    }
}


fileprivate struct Feed: Decodable {
    var entry: [AppModelDecodable]
}

struct AppModelNetwork: Decodable {
    private var feed: Feed
    
    var convertToAppModelList: [AppModel] {
        get {
            return self.feed.entry.compactMap {
                return try? AppModel(
                    title: $0.title.label,
                    artist: $0.artist.label,
                    name: $0.name.label,
                    imageList: $0.image.map { $0.label },
                    amount: $0.price.attributes?.amount,
                    currency: $0.price.attributes?.currency,
                    summary: $0.summary.label,
                    category: $0.category.attributes?.label,
                    rights: $0.rights.label,
                    releaseDate: $0.releaseDate.attributes.label
                )
            }
        }
    }
}
