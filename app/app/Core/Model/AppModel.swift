//
//  AppModel.swift
//  app
//
//  Created by Dario Ciaudano on 18/07/22.
//

import Foundation

struct AppModel {
    var title: String
    var artist: String
    var name: String
    var image: URL
    var amount: Double
    var currency: String
    var summary: String
    var category: String
    var rights: String
    var releaseDate: String
    
    
    init(
        title: String,
        artist: String,
        name: String,
        imageList: [String],
        amount: String?,
        currency: String?,
        summary: String,
        category: String?,
        rights: String,
        releaseDate: String
    ) throws {
        self.title = title
        self.artist = artist
        self.name = name
        
        if let urlString = imageList.last,
           let url = URL(string: urlString) {
            self.image = url
        } else {
            throw CustomError.unableToParseURL(url: imageList.last)
        }
        
        self.amount = Double(amount ?? "") ?? 0.0
        self.currency = currency ?? (Locale.current.currencyCode ?? "USD")
        
        self.summary = summary
        self.category = category ?? "---"
        self.rights = rights

        self.releaseDate = releaseDate
    }
}
