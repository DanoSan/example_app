//
//  CustomError.swift
//  app
//
//  Created by Dario Ciaudano on 18/07/22.
//

enum CustomError: Error, CustomStringConvertible {
    case invalidPath(url: String)
    case missingConnection
    case badURL
    case errorWith(code: Int, desc: String)
    case error(desc: String)
    case unableToParseURL(url: String?)
    case unexpectedError
    case unableToRetrieveItemCellAt(pos: Int)
    
    
    var description: String {
        get {
            switch(self) {
            case .invalidPath(let url):
                return "Invalid Path for \(url)"
            case .missingConnection:
                return "Lost internet connection"
            case .badURL:
                return "URL rerquest malformed"
            case .errorWith(let code, let desc):
                return "Error with code: \(code) and description: \(desc)"
            case .error(let desc):
                return "Error with description: \(desc)"
            case .unableToParseURL(let url):
                return "Unable to parse URL \(url ?? "--")"
            case .unexpectedError:
                return "Unexpected error"
            case .unableToRetrieveItemCellAt(let pos):
                return "Unable to retrieve Item Cell at pos \(pos)"
            }
        }
    }
}
