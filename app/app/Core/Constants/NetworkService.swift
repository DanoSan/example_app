//
//  NetworkService.swift
//  app
//
//  Created by Dario Ciaudano on 18/07/22.
//

enum NetworkService {
    case retrieveTopApplication(limit: Int)
    
    var servive: String {
        get {
            switch(self) {
            case .retrieveTopApplication(let limit):
                return "/us/rss/toppaidapplications/limit=\(limit)/json"
            }
        }
    }
    
}
