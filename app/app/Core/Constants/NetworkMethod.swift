//
//  NetworkMethod.swift
//  app
//
//  Created by Dario Ciaudano on 18/07/22.
//

enum NetworkMethod: String {
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
    case DELETE = "DELETE"
}
