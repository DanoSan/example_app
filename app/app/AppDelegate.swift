//
//  AppDelegate.swift
//  app
//
//  Created by Dario Ciaudano on 18/07/22.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let identifier = "Main"
        
        let host = "https://itunes.apple.com"
        let limit = 200
        
        self.window = UIWindow()
        
        let storyboard = UIStoryboard(name: identifier, bundle: nil)

        if let rootViewController = storyboard.instantiateViewController(withIdentifier: identifier) as? MainViewController {
            let navigationController = UINavigationController(rootViewController: rootViewController)

            rootViewController.viewModel = MainViewModel(
                NetworkEngine(host: host),
                elementsLimit: limit
            )
            
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }
}

